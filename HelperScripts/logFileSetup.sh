#!/bin/bash

#Set color parameters
RED='\033[0;31m'
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
NC='\033[0m' #No color

echo "Beginning setup of logs";
echo "-----------------------------";
if [[ $EUID -ne 0 ]]; then
   echo -e "${RED}ERROR:${NC} This script must be run as root"
   echo -e "${RED}EXITING INCOMPLETE${NC}"
   exit 1
fi

#using properties to derive user name
PropertyLocation="/home/tmp/hashList.properties"

while IFS='' read -r line || [[ -n "$line" ]]; do
if [[ ${line} = *"userName="* ]]; then
userName=$( cut -d "=" -f 2 <<< ${line})
fi
done < ${PropertyLocation}

printf "\nThis system will default to logging in /home/${userName}/logs.  Creating Directory now\n"

mkdir /home/${userName}/logs
echo "--Sql dump file" >> /home/${userName}/logs/sqlDump.txt
chown -R www-data:www-data /home/${userName}/logs
chgrp -R ${userName} /home/${userName}/logs
chmod -R 766 /home/${userName}/logs

printf "\n\nlogLocation = /home/${userName}/logs" >> ${PropertyLocation}

echo "Log Files set up successfully";
echo "DONE";
echo "-----------------------------";