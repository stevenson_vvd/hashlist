#!/bin/bash

#Set color parameters
RED='\033[0;31m'
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
NC='\033[0m' #No color

#set the properties for sql user/pass
PropertyLocation="/home/tmp/hashList.properties"

printf "\n\n"
echo "Beginning setup of Mysql database";
echo "-----------------------------";
if [[ $EUID -ne 0 ]]; then
   echo -e "${RED}ERROR:${NC} This script must be run as root"
   echo -e "${RED}EXITING INCOMPLETE${NC}"
   exit 1
fi

# replace "-" with "_" for database username
echo "Please set a name for the database"
read MAINDB;

# create password
echo "Please Set password for Mysql user linked to the new DB"
read PASSWDDB;


# If /root/.my.cnf exists then it won't ask for root password
if [ -f /root/.my.cnf ]; then

    mysql -e "CREATE DATABASE ${MAINDB} /*\!40100 DEFAULT CHARACTER SET utf8 */;"
    mysql -e "CREATE USER ${MAINDB}@localhost IDENTIFIED BY '${PASSWDDB}';"
    mysql -e "GRANT ALL PRIVILEGES ON ${MAINDB}.* TO '${MAINDB}'@'localhost';"
    mysql -e "FLUSH PRIVILEGES;"

# If /root/.my.cnf doesn't exist then it'll ask for root password
else
    echo "Please enter root user MySQL password!"
    read rootpasswd
    mysql -uroot -p${rootpasswd} -e "CREATE DATABASE ${MAINDB} /*\!40100 DEFAULT CHARACTER SET utf8 */;"
    mysql -uroot -p${rootpasswd} -e "CREATE USER ${MAINDB}@localhost IDENTIFIED BY '${PASSWDDB}';"
    mysql -uroot -p${rootpasswd} -e "GRANT ALL PRIVILEGES ON ${MAINDB}.* TO '${MAINDB}'@'localhost';"
    mysql -uroot -p${rootpasswd} -e "FLUSH PRIVILEGES;"
fi

# Create the Table and columns to hold all data
mysql -u${MAINDB} -p${PASSWDDB} -e "CREATE TABLE  ${MAINDB}.HashListTMP (
    SHA1 varchar(255),
    MD5 varchar(255),
    CRC32 varchar(255),
    FileName varchar(255),
    FileSize INTEGER,
    ProductCode BIGINT,
    OpSystemCode varchar(255),
    SpecialCode varchar(255)
);";

#The final table
mysql -u${MAINDB} -p${PASSWDDB} -e "CREATE TABLE ${MAINDB}.HashList (
    SHA1 varchar(255),
    MD5 varchar(255),
    CRC32 varchar(255),
    FileName varchar(255),
    FileSize INTEGER,
    ProductCode BIGINT,
    OpSystemCode varchar(255),
    SpecialCode varchar(255),
    Version INTEGER,
    PRIMARY KEY (FileName,ProductCode,Version)
);";

#The holder table for distinct values
mysql -u${MAINDB} -p${PASSWDDB} -e "CREATE TABLE ${MAINDB}.temp_rec (
    SHA1 varchar(255),
    MD5 varchar(255),
    CRC32 varchar(255),
    FileName varchar(255),
    FileSize INTEGER,
    ProductCode BIGINT,
    OpSystemCode varchar(255),
    SpecialCode varchar(255),
    Version INTEGER
);";

echo "Storing user/pass for sql in properties";

printf "\n#The user/db for sql commands" >> ${PropertyLocation};
printf "\nsqlUser=${MAINDB}" >> ${PropertyLocation};
printf "\n\n#The sql user password" >> ${PropertyLocation};
printf "\nsqlPass=${PASSWDDB}" >> ${PropertyLocation};
printf "\n"