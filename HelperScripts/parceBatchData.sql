CREATE DEFINER=`9B_DB`@`%` PROCEDURE `ParceBatchData`()
BEGIN
-- de dup
insert into temp_rec (MD5, SHA1, CRC32, FileName, ProductCode, SpecialCode,FileSize, OpSystemCode, Version) 
select Distinct MD5, SHA1, CRC32, FileName, ProductCode, SpecialCode,FileSize, OpSystemCode, -1 from HashListTMP; 

-- remove data from original table to prep for new table
delete from HashListTMP; 


-- add the rows that don't exst 
insert into HashList (MD5, SHA1, CRC32, FileName, ProductCode, SpecialCode,FileSize, OpSystemCode, Version) 
select MD5, SHA1, CRC32, FileName, ProductCode, SpecialCode,FileSize, OpSystemCode, 1  from temp_rec tr
where NOT EXISTS
(
Select * from HashList hl where 
tr.FileName = hl.FileName
and
hl.ProductCode = tr.ProductCode
        AND SHA1 = hl.SHA1
        AND MD5 = hl.MD5
        AND CRC32 = hl.CRC32
        AND FileSize = hl.FileSize
        AND OpSystemCode = hl.OpSystemCode
        AND SpecialCode = hl.SpecialCode
);

-- remove the rows from temp that we just added to HashList
DELETE FROM temp_rec 
WHERE
    NOT EXISTS( SELECT 
        *
    FROM
        HashList hl
    WHERE
        FileName = hl.FileName
        AND hl.ProductCode = ProductCode
        AND SHA1 = hl.SHA1
        AND MD5 = hl.MD5
        AND CRC32 = hl.CRC32
        AND FileSize = hl.FileSize
        AND OpSystemCode = hl.OpSystemCode
        AND SpecialCode = hl.SpecialCode);


-- update the version info in the de dup table
UPDATE temp_rec AS tr
        INNER JOIN
    HashList AS hl ON hl.ProductCode = tr.ProductCode 
SET 
    tr.Version = (SELECT 
            hl2.Version
        FROM
            HashList AS hl2
                INNER JOIN
            (SELECT 
                *
            FROM
                temp_rec) AS tr2 ON hl2.ProductCode = tr2.ProductCode
        WHERE
            hl2.FileName = tr2.FileName
        ORDER BY Version DESC
        LIMIT 1) + 1
WHERE
    hl.FileName = tr.FileName
    AND hl.ProductCode = tr.ProductCode
        AND (hl.MD5 <> tr.MD5
        OR hl.SHA1 <> tr.SHA1
        OR hl.CRC32 <> tr.CRC32
        OR hl.FileSize <> tr.FileSize
        OR hl.OpSystemCode <> tr.OpSystemCode
        OR hl.SpecialCode <> tr.SpecialCode);
        
-- insert the values into the final table 
insert into HashList (MD5, SHA1, CRC32, FileName, ProductCode, SpecialCode,FileSize, OpSystemCode, Version) 
select MD5, SHA1, CRC32, FileName, ProductCode, SpecialCode,FileSize, OpSystemCode, Version  from temp_rec tr
where Version <> -1; 


-- remove the rest of the rows added
DELETE FROM temp_rec; 

-- commit; 

commit; 
END