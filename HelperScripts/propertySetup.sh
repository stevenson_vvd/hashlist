#!/bin/bash

#Set color parameters
RED='\033[0;31m'
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
NC='\033[0m' #No color

#using properties to derive user name
PropertyLocation="/home/tmp/hashList.properties"

echo "------------";
echo "Creating Properties file";
echo  "------------";
echo "NOTE: The properties file will live in /home/tmp and CANNOT BE MOVED";
printf "\nEnter the name of the linux user who will own the log/sql files (should not be root): "
read username;
echo "Writing username to properties file"
printf "\nuserName=${username}" >> ${PropertyLocation}
echo "DONE"

