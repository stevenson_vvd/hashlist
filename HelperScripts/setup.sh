#!/bin/bash

#Set color parameters
RED='\033[0;31m'
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
NC='\033[0m' #No color

echo "Beginning setup for Hash List Ingest Server (HLIS)";
echo "-----------------------------";
if [[ $EUID -ne 0 ]]; then
   echo -e "${RED}ERROR:${NC} This script must be run as root"
   echo -e "${RED}EXITING INCOMPLETE${NC}"
   exit 1
fi
chmod 755 /home/tmp/*
bash /home/tmp/propertySetup.sh
bash /home/tmp/logFileSetup.sh
bash /home/tmp/mySqlSetup.sh

echo "----------------";
echo "FINISHED SETUP";
echo "----------------";