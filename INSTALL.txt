1 - Set up LAMP stack using instructions from https://www.digitalocean.com/community/tutorials/how-to-install-linux-apache-mysql-php-lamp-stack-ubuntu-18-04
(NOTE: let's make some of this a script for bash)
2 - Copy over server files (this should be both a ps1 and a .sh script) to /home/tmp on Linux (mkdir if this directory does not exist)
3 - install open ssh on Linux server
4 - set php options for large file uploads
5 - Run the following commands to set permissions on files on script files (Note: <linux User> refers to a non-root user who you wish to be able to run the commands)
    ~su
    <enter root password>
    ~chmod 755 /home/tmp
    ~chown -R <linux User> /home/tmp
    ~chgrp -R <linux User> /home/tmp
6 - run the setup.sh file ( bash /home/tmp/setup.sh)
NOTE: The default name (user) for the database is 9B_DB, IF YOU CHANGE THIS YOU MUST UPDATE THE STORED PROCEDURE HEADER
7 - add the contents of parceBatchData.sql as a stored procedure titled ParceBatchData in your mysql database.