<h1>README</h1>

This project is built by dstevenson@vvdev.net.  All contents of this project are the property
of the owner unless otherwise described.  All externally downloaded libraries used in this project will be listed in the
EXTERNAL_LIBRARIES.txt readme.

<hr>
<h2> Query Mediator </h2>
The query mediator exposes query funcionality via pre-written queries.
<br> <h3>Query LIST: </h3>
NOTE: All queries take "productNo" as an additional constraint included in the url parameters<br>
<br>
"mostRecentVersion":
<br>- will return the highest version of whatever file name is passed
<br>
<br>"fileList": if set =true in parameters, returns all distinct file names in DB, ordered by File name
 .  If set =(some string) will look for all distinct file names containing that string and return them.
  <br>
  NOTE: there is a limit of 5000 rows returned;
  <br>