import { Routes } from '@angular/router';
import {FileUploadComponent} from './file-upload/file-upload.component';
import {PageNotFoundComponent} from "./page-not-found/page-not-found.component";
import {LandingComponent} from "./landing/landing.component";
import {QueryComponent} from "./query/query.component";

export const routes: Routes = [
  { path: '', component: LandingComponent },
  { path: 'upload', component: FileUploadComponent },
  { path: 'query', component: QueryComponent },
  { path: '**', component: PageNotFoundComponent }
];

