import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {

  files;
  processingIsVisible:boolean = false;

  constructor(private router:Router) {
  }


  ngOnInit() {
  }

  onChange(event) {
    this.files = event.target.files;
    console.log(this.files);
  }

  routeHome() {
    this.router.navigate(['']);
  }

  submitFile(event) {
    event.preventDefault();
       //console.log("current file: " + this.files[0]);
    if (this.files != null) {
     this.sendFormData();
    }
    else {
      alert("Please select a file to upload")
    }
    return false;
  }


  sendFormData(){
    var formData = new FormData();
    this.processingIsVisible = true;
    this.changeOpacity();
    formData.append("uploadfile", this.files[0]);
    var url = '/assets/php/hashIngester.php?name=' + this.files[0].name;
    let req = new XMLHttpRequest();
    req.open('POST', url);
    // console.log(req.toString());
    req.onload = () => {
      //parse and format
      console.log("response: \n" + req.response);
      this.processingIsVisible = false;
      this.changeOpacity();
      if (req.response.indexOf("Error") > -1) {
        alert("Error in file upload")
      }
      else {
        alert("File Uploaded Successfully");
      }
    };
    req.send(formData);
  }

  changeOpacity() {
    var elm = document.getElementById("fileUpload");

    if (this.processingIsVisible) {
      elm.style.opacity = "0.5";
    } else {
      elm.style.opacity = "1";
    }
  }

}
