import {Component} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent  {

  constructor(private router:Router) {
  }

  routeUpload() {
    this.router.navigate(['upload']);
  }

  routeQueryHome() {
    this.router.navigate(['query']);
  }
}
