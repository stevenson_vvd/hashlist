import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-query',
  templateUrl: './query.component.html',
  styleUrls: ['./query.component.css']
})
export class QueryComponent {
  processingIsVisible:boolean = false;
  filesIsVisible:boolean = false;
  versionIsVisible:boolean = false;
  diffIsVisible:boolean = false;
  fileName:string = '';
  version:string;
  files;


  constructor(private router:Router) {
  }

  routeHome() {
    this.router.navigate(['']);
  }


  getAllFiles() {
    this.processingIsVisible = true;
    var params;
    var NL = (<HTMLInputElement>document.getElementById("NameLike")).value;
    if (NL != null &&
      NL != '') {
      params = '?fileList=' + NL;
    } else {
      params = '?fileList=true';
    }
    var PID = (<HTMLInputElement>document.getElementById("PID")).value;
    if (PID != null &&
      PID != '') {
      params += '&productNo=' + PID
    }

    var url = '/assets/php/QueryMediator.php';
    let req = new XMLHttpRequest();
    req.open('POST', url + params);
    // console.log(req.toString());
    req.onload = () => {
      this.processingIsVisible = false;
      if (req.response.indexOf("MYEXCEPTION") > -1) {
        console.log(req.response);
        alert("Error retrieving data")
      }
      else {
        this.files = (JSON.parse(req.response));
        this.filesIsVisible = true;
      }
    };
    req.send();
  }

  getHighestVersion() {
    this.processingIsVisible = true;
    var FN = (<HTMLInputElement>document.getElementById("FileName")).value;
    if (FN == null ||
      FN == '') {
      alert("File name cannot be empty for this query");
      this.processingIsVisible = false;
    } else {
      var params = '?mostRecentVersion=' + FN;
      var PID = (<HTMLInputElement>document.getElementById("PID")).value;
      if (PID != null &&
        PID != '') {
        params += '&productNo=' + PID
      }
      var url = '/assets/php/QueryMediator.php';
      let req = new XMLHttpRequest();
      req.open('POST', url + params);
      // console.log(req.toString());
      req.onload = () => {
        this.processingIsVisible = false;
        if (req.response.indexOf("MYEXCEPTION") > -1) {
          console.log(req.response);
          alert("Error retrieving data")
        }
        else {
          this.version = (JSON.parse(req.response))[0];
          this.fileName = FN;
          this.versionIsVisible = true;
        }
      };
      req.send();
    }
  }


  getVersionDiff() {
    this.processingIsVisible = true;
    var FN = (<HTMLInputElement>document.getElementById("DiffFileName")).value;
    var SV = (<HTMLInputElement>document.getElementById("DiffStartV")).value;
    var EV = (<HTMLInputElement>document.getElementById("DiffEndV")).value;
    if (FN == null ||
      FN == '' ||
      SV == null || SV == '' ||
      EV == null || EV == '') {
      alert("Required parameters cannot be empty for this query");
      this.processingIsVisible = false;
    } else {
      var params = '?versionDiff=true'
      params += '&fileName=' + FN;
      params += '&startVersion=' + SV;
      params += '&endVersion=' + EV;
      var PID = (<HTMLInputElement>document.getElementById("PID")).value;
      if (PID != null &&
        PID != '') {
        params += '&productNo=' + PID
      }
      var url = '/assets/php/QueryMediator.php';
      let req = new XMLHttpRequest();
      req.open('POST', url + params);
      // console.log(req.toString());
      req.onload = () => {
        this.processingIsVisible = false;
        if (req.response.indexOf("MYEXCEPTION") > -1) {
          console.log(req.response);
          alert("Error retrieving data")
        }
        else {
          var holder = (JSON.parse(req.response))[0];
          console.log(holder);
          this.fileName = FN;
          this.diffIsVisible = true;
        }
      };
      req.send();
    }
  }

}
