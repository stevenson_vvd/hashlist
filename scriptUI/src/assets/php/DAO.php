<?php
/**
 * Created by IntelliJ IDEA.
 * User: dstevens
 * Date: 6/27/2018
 * Time: 7:15 PM
 */
$propLocation = "/home/tmp/hashList.properties";

function getLogLocation()
{
  $logLocation = '';
  $propFile = fopen($GLOBALS['propLocation'], "r");
  while (!feof($propFile)) {
    $line = fgets($propFile);
    $exists = strpos($line, "logLocation");
    if ($exists !== false) {
      $logLocation = trim(substr($line, strpos($line, '=') + 1));
//      echo "\nLog location: " . $GLOBALS['logLocation'];
    }
  }
  fclose($propFile);
  return $logLocation;
}

function getSqlUser()
{
  $user = '';
  $propFile = fopen($GLOBALS['propLocation'], "r");
  while (!feof($propFile)) {
    $line = fgets($propFile);
    $exists = strpos($line, "sqlUser");
    if ($exists !== false) {
      $user = trim(substr($line, strpos($line, '=') + 1));
//      echo "\nSql User: " . $GLOBALS['sqlUser'];
    }
  }
  fclose($propFile);
  return $user;
}

function getSqlPass()
{
  $pass = '';
  $propFile = fopen($GLOBALS['propLocation'], "r");
  while (!feof($propFile)) {
    $line = fgets($propFile);
    $exists = strpos($line, "sqlPass");
    if ($exists !== false) {
     $pass = trim(substr($line, strpos($line, '=') + 1));
//      echo "\nSql Pass: " . $GLOBALS['sqlPass'];
    }
  }
  fclose($propFile);
  return $pass;
}

function connectToDb() {
  $user = getSqlUser();
  $pass = getSqlPass();
  $connection = mysqli_connect("127.0.0.1",$user, $pass, $user);

  if (!$connection) {
    return "Error: Unable to connect to MySQL." . PHP_EOL;
//    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;User
//    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
  } else {
    $connection->query('SET GLOBAL wait_timeout=28800');
    $connection->query('SET GLOBAL interactive_timeout=28800');
  }
  
  return $connection;
}

function commitAndCloseDB($connection){
  /* commit transaction */
  if (!$connection->commit()) {
    echo "Transaction commit failed\n";
  }


  mysqli_close($connection);
}

