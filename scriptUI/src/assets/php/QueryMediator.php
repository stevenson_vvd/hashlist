<?php
/**
 * Created by IntelliJ IDEA.
 * User: dstevens
 * Date: 6/27/2018
 * Time: 7:26 PM
 */

include "DAO.php";

try {
  // ---- Get most recent file version ------ //
  if ($_GET["mostRecentVersion"] !== null &&
    $_GET["mostRecentVersion"] !== ""
  ) {
    if ($_GET["productNo"] !== null &&
      $_GET["productNo"] !== ""
    ) {
      getMostRecentVersionProd($_GET["mostRecentVersion"], $_GET["productNo"]);
    } else {
      getMostRecentVersion($_GET["mostRecentVersion"]);
    }
  }

  // ---- Get list of all files ------ //
  if ($_GET["fileList"] !== null &&
    $_GET["fileList"] !== ""
  ) {
    //basic sql injection preventative
    if (strpos($_GET["fileList"], ';') !== false) {
      echo "MYEXCEPTION ERROR: INVALID PARAMETER VALUE";
    }

    if (strcasecmp($_GET["fileList"], "true") == 0) {
      if ($_GET["productNo"] !== null &&
        $_GET["productNo"] !== ""
      ) {
        getAllFilesProd($_GET["productNo"]);
      } else {
        getAllFiles();
      }
    } else {
      getFilesWithNameLike($_GET["fileList"]);
    }
  }

  // ---- Get list of differences in a file ------ //
  if ($_GET["versionDiff"] !== null &&
    $_GET["versionDiff"] !== ""
  ) {
    //basic sql injection preventative
    if (strpos($_GET["versionDiff"], ';') !== false) {
      echo "MYEXCEPTION ERROR: INVALID PARAMETER VALUE";
    }

    if (strcasecmp($_GET["versionDiff"], "true") == 0) {
      $startV = $_GET['startVersion'];
      $endV = $_GET['endVersion'];
      $versionFName = $_GET['fileName'];

      if ($startV != null &&
        $endV != null &&
        $versionFName != null &&
        $startV != '' &&
        $endV != '' &&
        $versionFName != ''
      ) {
        if ($_GET["productNo"] !== null &&
          $_GET["productNo"] !== ""
        ) {
          getDiffInVersionProd($versionFName,$_GET['productNo'], $startV, $endV);
        } else {
          getDiffInVersion($versionFName, $startV, $endV);
        }
      }
    }
  }

} catch (Exception $ex) {
  echo "There was an error running the query";
}

function getMostRecentVersion($fileName)
{

  if (strpos($fileName, ';') !== false) {
    return "ERROR: INVALID FILE NAME";
  }
  $DB = getSqlUser();

  $VERSION_QUERY = "Select Version from " . $DB . ".HashList where FileName = " . $fileName . " order by Version desc Limit 1";
  $link = connectToDb();

  if ($result = $link->query($VERSION_QUERY)) {
    commitAndCloseDB($link);
    processResults($result);
  } else {
    commitAndCloseDB($link);
    echo "MYEXCEPTION Error Executing Query";
  }
}

function getMostRecentVersionProd($fileName, $productNo)
{

  if (strpos($fileName, ';') !== false) {
    return "ERROR: INVALID FILE NAME";
  }
  $DB = getSqlUser();

  $VERSION_QUERY = "Select Version from " . $DB . ".HashList where FileName = " . $fileName . "  and ProductCode = " . $productNo . " order by Version desc Limit 1";
  $link = connectToDb();

  if ($result = $link->query($VERSION_QUERY)) {
    commitAndCloseDB($link);
    processResults($result);
  } else {
    commitAndCloseDB($link);
    echo "MYEXCEPTION Error Executing Query";
  }
}

function getAllFiles()
{
  $DB = getSqlUser();
  $VERSION_QUERY = "Select DISTINCT FileName from " . $DB . ".HashList order by FileName limit 5000";
  $link = connectToDb();

  if ($result = $link->query($VERSION_QUERY)) {
    commitAndCloseDB($link);
    processResults($result);
  } else {
    commitAndCloseDB($link);
    echo " MYEXCEPTIONError Executing Query";
  }
}

function getAllFilesProd($prodId)
{
  $DB = getSqlUser();

  $VERSION_QUERY = "Select DISTINCT FileName from " . $DB . ".HashList where ProductCode = " . $prodId . " order by FileName limit 5000 ";
  $link = connectToDb();

  if ($result = $link->query($VERSION_QUERY)) {
    commitAndCloseDB($link);
    processResults($result);
  } else {
    commitAndCloseDB($link);
    echo "MYEXCEPTION Error Executing Query";
  }
}

function getFilesWithNameLikeProd($nameLike, $prodId)
{
  $DB = getSqlUser();

  $VERSION_QUERY = "Select DISTINCT FileName from " . $DB . ".HashList where FileName like '%" . $nameLike . "%'  and ProductCode = " . $prodId . " order by FileName limit 5000";
  $link = connectToDb();

  if ($result = $link->query($VERSION_QUERY)) {
    commitAndCloseDB($link);
    processResults($result);
  } else {
    commitAndCloseDB($link);
    echo "MYEXCEPTION Error Executing Query";
  }
}

function getFilesWithNameLike($nameLike)
{
  $DB = getSqlUser();

  $VERSION_QUERY = "Select DISTINCT FileName from " . $DB . ".HashList where FileName like '%" . $nameLike . "%' order by FileName limit 5000";
  $link = connectToDb();

  if ($result = $link->query($VERSION_QUERY)) {
    commitAndCloseDB($link);
    processResults($result);
  } else {
    commitAndCloseDB($link);
    echo "MYEXCEPTION Error Executing Query";
  }
}

function processResults($result)
{
  $retArr = array();
  while ($row = $result->fetch_row()) {
    array_push($retArr, $row[0]);
  }
  echo json_encode($retArr);
}

function getDiffInVersionProd($fileName, $prodId, $versionStart, $versionEnd)
{
  if (strpos($fileName, ';') !== false) {
    return "ERROR: INVALID FILE NAME";
  }
  $DB = getSqlUser();

  $VERSION_QUERY = "Select Version from " . $DB . ".HashList where FileName = " . $fileName . "AND ProductCode = " . $prodId .
    " AND VERSION Between " . $versionStart . " AND " . $versionEnd . " order by Version desc Limit 5000";
  $link = connectToDb();

  if ($result = $link->query($VERSION_QUERY)) {
    commitAndCloseDB($link);
    processResults($result);
  } else {
    commitAndCloseDB($link);
    echo "MYEXCEPTION: Error Executing Query";
  }

}

function getDiffInVersion($fileName, $versionStart, $versionEnd)
{
  if (strpos($fileName, ';') !== false) {
    return "ERROR: INVALID FILE NAME";
  }
  $DB = getSqlUser();

  $VERSION_QUERY = "Select Version from " . $DB . ".HashList where FileName = " . $fileName .
    " AND VERSION Between " . $versionStart . " AND " . $versionEnd . " order by Version desc Limit 5000";
  $link = connectToDb();

  if ($result = $link->query($VERSION_QUERY)) {
    commitAndCloseDB($link);
    processResults($result);
  } else {
    commitAndCloseDB($link);
    echo "MYEXCEPTION Error Executing Query";
  }
}

