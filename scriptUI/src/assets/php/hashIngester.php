<?php
include "DAO.php";
$importFile = null;
$INSERT_TABLE = 'HashListTMP';
$FINAL_TABLE = 'HashList';
$filename = $_GET['name'];
$batchSize = 500;
$count = 1;
$queryStr = '';
$queryArr = array();
$stringFinished = false;
$logLocation = '';
$sqlUser = '';

try {
  //echo 'file name: ' . $filename;
  //echo "\n".'files: '. print_r($_FILES);
  //echo "\n\n\n\n MAKE TEST FILE: \n" . substr(file_get_contents($_FILES["uploadfile"]["tmp_name"]), 0, 10000). "\n\n";
  $logLocation = getLogLocation();
  $sqlUser = getSqlUser();
} catch (Exception $ex) {
  echo 'there was an exception making the tmp file', $ex->getMessage(), '';
}

try {
  $importFile = fopen($_FILES["uploadfile"]["tmp_name"], "r");
  $tagLine = fgets($importFile);
  $tagLine = str_replace("\"", "", $tagLine);
  $tagLine = str_replace("-", "", $tagLine);
  $holderArr = array();
  //echo 'Tag line: '. $tagLine;
  while (!feof($importFile)) {

    // map the file given to the array
    $csv = fgets($importFile);
    $csv = explode(",", $csv);
    //echo "\n" . 'csv array: ' . print_r(array_values($csv));
    if (count($csv) < 7) {
      //we don't have enough entries. skip this one and move on to the next
      //if this one has a strange new line, let's save it and hold it for the next line
      if (count($holderArr) == 0) {
        $holderArr = $csv;
      } else {
        // we already put something in the holder array. let's see if we can append it
        array_push($holderArr, $csv);
        if (count($csv) == 7) {
          createQueryFromCSVArray();
        } else {
          echo "Was unable to generate query for line " . $count . " of batch : " . count($queryArr);
        }
      }
    } else {
      createQueryFromCSVArray();
    }
  }

  pushRemainderToArray();
  runQueries();

  //echo "\nQuery string: " . $queryStr;
  //echo "\n Legnth of Query array: " . count($GLOBALS['queryArr']);
  fclose($importFile);
} catch (Exception $ex) {
  return  'ERROR';
}

//------- Helper functions ----- //
function runQueries()
{
  //make the query happen
  //TODO: Authenticate the user for this information
  $link = connectToDb();
  
//  echo "Success: A proper connection to MySQL was made! The my_db database is great." . PHP_EOL;
//  echo "Host information: " . mysqli_get_host_info($link) . PHP_EOL;
//run the query
  for ($i = 0; $i < count($GLOBALS['queryArr']); $i++) {

    if ($link->query($GLOBALS['queryArr'][$i]) !== TRUE) {
      //echo "\nDEBUG: FAILED QUERY : " . $GLOBALS['queryArr'][$i];
      echo "query " . ($i + 1) . " of " . count($GLOBALS['queryArr']) . " Failed adding to TMP DB Adding to dump file";

      $log = fopen($GLOBALS['logLocation'] . "/sqlDump.txt", "a");
      fwrite($log, $GLOBALS['queryArr'][$i]);
      fclose($log);
    }

    //call the stored procedure to update the data


    if ($link->query("CALL " . $GLOBALS['sqlUser'] . ".ParceBatchData") !== TRUE) {
      //echo "\nDEBUG: FAILED QUERY : " . $GLOBALS['queryArr'][$i];
      echo "Failed to execute stored procedure";

      //    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;User
//    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    }
  }

  commitAndCloseDB($link);
}

function createQueryFromCSVArray()
{
  //if we are starting the batch, make the statement start
  if ($GLOBALS['count'] == 1) {
    //don't need column names for properly formatted data
    $GLOBALS['queryStr'] .= "INSERT INTO " . $GLOBALS['INSERT_TABLE'] . " ( " . $GLOBALS['tagLine'] . " ) values";
  }

  if ($GLOBALS['count'] > 1 && $GLOBALS['count'] <= $GLOBALS['batchSize']) {
    //if we are on our next pass, we need a comma
    $GLOBALS['queryStr'] .= ",";
  }

  //for each line in the csv array
  for ($v = 0; $v < count($GLOBALS['csv']); $v++) {
    //add the values from this location in the array
    //if v is 0, add the opening parenthesis
    addValuesToQueryString($v);
  }
  if ($GLOBALS['count'] == $GLOBALS['batchSize']) {
    finishQueryString();
    //submit the query and start a new batch
    //push the query to the array to be ran as a batch
    array_push($GLOBALS['queryArr'], $GLOBALS['queryStr']);
    $GLOBALS['queryStr'] = '';
    //echo "\nQuery String: " . $GLOBALS['queryStr'];
    //reset the query string and batch values
    $GLOBALS['count'] = 0;

  }
  $GLOBALS['count']++;
  //now we have added ONE line to the query string, the wile will now cause to repeat
}

function finishQueryString()
{
  if (!$GLOBALS['stringFinished']) {
    $GLOBALS['queryStr'] = str_replace("\"", "'", $GLOBALS['queryStr']);
    $GLOBALS['queryStr'] .= ";";
    $GLOBALS['stringFinished'] = true;
  }
}

function pushRemainderToArray()
{
  //we have hit the end of the file, submit the final query:
  array_push($GLOBALS['queryArr'], $GLOBALS['queryStr']);
}

function addValuesToQueryString($iterator)
{
  if ($iterator == 0) {
    $GLOBALS['queryStr'] .= "(";
  }

  $GLOBALS['queryStr'] .= $GLOBALS['csv'][$iterator];

  if ($iterator < count($GLOBALS['csv']) - 1) {
    $GLOBALS['queryStr'] .= ",";
  }
  //if v is last, add closing parenthesis
  if ($iterator == count($GLOBALS['csv']) - 1) {
    $GLOBALS['queryStr'] .= ")";
    if (feof($GLOBALS['importFile'])) {
      finishQueryString();
    }
  }
}
?>
